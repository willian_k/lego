"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const roles = require("./roles");
const express = require("express");
const mw = require("promise-mw-2");
const api = require("./api");
const auth_mw_1 = require("@components/auth-mw");
exports._router = express.Router();
const user = mw.wrap(exports._router);
user.get('/login', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (req.session.userId)
        return res.other('redirect', '/pages/user/login');
    return res.render('v-login', {
        title: 'Login',
        //show new account button if theres no admin user or if the option is enabled
        showNewAccount: /*config.new_account_button ||*/ roles.USER_ADMIN_ID === undefined
    });
}));
user.get('/password-reset-request', (req, res) => __awaiter(this, void 0, void 0, function* () {
    return res.render('v-password-reset-request');
}));
user.get('/password-reset-confirm/:token', (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.ensureHas(req.params, ['token']);
    const token = req.params.token;
    let password = yield api.passwordResetConfirm(token);
    return res.render('v-password-reset-confirm', { password });
}));
user.get('/password-change', auth_mw_1.authGate, (req, res) => __awaiter(this, void 0, void 0, function* () {
    return res.render('v-password-change', { title: 'Alterar senha' });
}));
