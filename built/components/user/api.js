"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const mw = require("promise-mw-2");
const roles = require("./roles");
const bcrypt = require("bcrypt");
const auth_mw_1 = require("@components/auth-mw");
const promisify_1 = require("promisify");
const passwordGen = require("password-generator");
const uuid = require("uuid/v1");
const mailer = require("@components/mail");
exports._router = express.Router();
const api = mw.wrap(exports._router);
const HTTP = mw.STATUS_CODES;
function userLogin({ username, password, session }) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!username || !password)
            throw Error('Falta o nome de usuário ou senha.');
        if (session.userId)
            throw Error('Primeiro faça o logout da sessão atual.');
        let [found] = yield roles.User.find({ name: username }).select({ hash: 1 }).limit(1).exec();
        if (!found)
            throw new mw.Error(HTTP.UNAUTHORIZED, 'Credenciais inválidas.');
        let result = yield bcrypt.compare(password, found.hash);
        if (!result)
            throw new mw.Error(HTTP.UNAUTHORIZED, 'Credenciais inválidas.');
        session.userId = found._id;
        session.userName = found.name;
        return { status: 'OK' };
    });
}
api.post('/login', (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.ensureHas(req.body, ['username', 'password']);
    let resp = yield userLogin({
        username: req.body.username,
        password: req.body.password,
        session: req.session
    });
    return res.send(resp);
}));
api.get('/logout', auth_mw_1.authGate, (req, res) => __awaiter(this, void 0, void 0, function* () {
    yield promisify_1.promisify(req.session.destroy);
    return res.send({ status: 'OK' });
}));
function passwordResetConfirm(_token) {
    return __awaiter(this, void 0, void 0, function* () {
        let [event] = yield roles.EventToken.find({ event: 'pasword-reset-confirm', token: _token }).exec();
        if (!event)
            throw Error('Falha ao encontrar token para esse pedido.');
        let newpass = passwordGen(12);
        let newHash = yield bcrypt.hash(newpass, 10);
        yield roles.User.update({ _id: event._id }, { $set: { hash: newHash } }).exec();
        yield event.remove();
        return newpass;
    });
}
exports.passwordResetConfirm = passwordResetConfirm;
api.post('/password-reset-confirm', (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.ensureHas(req.body, ['token']);
    let newpass = yield passwordResetConfirm(req.body.token);
    return res.send({ password: newpass });
}));
api.post('/password-reset-request', (req, res) => __awaiter(this, void 0, void 0, function* () {
    res.ensureHas(req.body, ['email']);
    const [user] = yield roles.User.find({ email: req.body.email }).aggregate([
        {
            $lookup: {
                from: 'User',
                localField: "_id",
                foreignField: 'user',
                as: 'EventToken'
            }
        }
    ]).exec();
    if (!user)
        return res.send({});
    if (!user.token)
        throw Error('Já existe um pedido pendente para este usuário.');
    const tokenToSave = uuid();
    yield new roles.EventToken({
        user: user._id,
        event: 'password-reset-confirm',
        token: tokenToSave
    }).save();
    yield mailer.send({
        email: req.body.email,
        subject: 'Redefinição de senha',
        content: PWD_RESET_CONTENT(user.name, tokenToSave)
    });
    return res.send({ token: tokenToSave });
}));
const PWD_RESET_CONTENT = (username, bytes) => `**Olá ${username}**,
Acesse o link ${process.env.APP_BASE_URL}/pages/user/password-reset-confirm/${bytes}
para redefinir a sua senha.`;
api.post('/create', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (roles.USER_ADMIN_ID !== undefined)
        throw Error('No momento, não é possível adicionar um novo editor.');
    res.ensureHas(req.body, ['username', 'email']);
    let { username, email } = req.body;
    if (!username || !email)
        throw Error('Falta o nome de usuário ou o email.');
    let _password = passwordGen(12);
    let hash = yield bcrypt.hash(_password, 10);
    //TODO: multiple editors
    let setRole = roles.USER_ADMIN_ID === undefined ? roles.ROLE_ADMIN_ID : roles.ROLE_NEW_ID;
    yield new roles.User({
        name: username, email: email, hash: hash, role: setRole
    }).save();
    yield mailer.send({
        email,
        subject: 'Nova conta',
        content: NEW_USER_CONTENT(username, _password)
    });
    return res.send({ status: 'ok' });
}));
const NEW_USER_CONTENT = (username, _password) => `Olá! Essas são suas credenciais:
**Nome de usuário**: ${username}
**Senha:** ${_password}`;
api.post('/password-change', auth_mw_1.authGate, (req, res) => __awaiter(this, void 0, void 0, function* () {
    var b = req.body;
    res.ensureHas(b, ['currentPassword', 'newPassword', 'newPasswordConfirm']);
    if (b.newPassword !== b.newPasswordConfirm)
        throw Error('As novas senhas devem bater.');
    let [user] = yield roles.User.find({ _id: req.session.userId }).select({ hash: 1 }).limit(1).exec();
    if (!user)
        throw Error('Erro Inesperado!');
    let compareresult = bcrypt.compare(b.currentPassword, user.hash);
    if (!compareresult)
        throw Error('Senha incorreta.');
    let hashed = bcrypt.hash(b.newPassword, 10);
    yield user.update({ $set: { hash: hashed } });
    return res.send({ status: 'ok' });
}));
