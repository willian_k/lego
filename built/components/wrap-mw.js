"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mw = require("promise-mw-2");
const express = require("express");
exports.createMwRouter = () => {
    const router = express.Router();
    let out = mw.wrap(router);
    out.router = router;
    return out;
};
exports.createMwApp = () => {
    const app = express();
    const out = mw.wrap(app);
    out.app = app;
    return out;
};
