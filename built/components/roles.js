"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
//role -----
const roleSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    isOwner: Boolean,
    isNew: Boolean
});
exports.Role = mongoose.model('roles', roleSchema);
exports.User = mongoose.model('user', new mongoose.Schema({
    role: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    hash: String,
    name: String
}));
//init -----
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        //create admin role
        let rows = yield exports.Role.find({}).count().exec();
        if (!rows) {
            console.log('Initializing roles (first run)...');
            exports.Role.create({
                name: 'admin',
                isOwner: true,
                isNew: true
            });
        }
        //define who has the key roles
        let rows2 = yield exports.Role.find({}).lean().exec();
        rows2.forEach(row => {
            if (row.isOwner)
                exports.ROLE_ADMIN_ID = row._id;
            else if (row.isNew)
                exports.ROLE_NEW_ID = row._id;
        });
        let rows3 = yield exports.User.find({ role: exports.ROLE_ADMIN_ID }).lean().exec();
        if (rows3.length)
            exports.USER_ADMIN_ID = rows3[0]._id;
    });
}
exports.init = init;
