"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const express = require("express");
exports.router = express.Router();
exports.router.use(bodyParser.json());
exports.router.use('/user', require('./user/api'));
function init() {
    return __awaiter(this, void 0, void 0, function* () {
        exports.router.use((error, req, res, next) => {
            console.error(error);
            res.status(error.statusCode || 500).send({
                error: {
                    message: error.message
                }
            });
        });
    });
}
exports.init = init;
