"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const ehbs = require("express-handlebars");
const glob = require("glob");
const Path = require("path");
exports.app = express();
exports.init = (globalopts) => {
    const opts = globalopts.pages || {};
    let hbsInstance = ehbs.create({
        extname: '.hbs',
        defaultLayout: opts.defaultLayout || 'main-layout',
        layoutsDir: opts.layoutsDir || globalopts.routesDir + '/_views/layouts',
        helpers: opts.helpers
    });
    // let moreRoutesDirs = opts.moreRoutesDirs || []
    // let moreDirs = moreRoutesDirs.reduce((out, current) => {
    //     let list = glob.sync(current + '/**/views')
    //     return [...out, ...list]
    // } ,[] as string[])
    hbsInstance.partialsDir = [
        globalopts.routesDir + './_views/partials',
        ...glob.sync(globalopts.routesDir + '/**/views'),
    ];
    exports.app.engine('.hbs', hbsInstance.engine);
    exports.app.set('view engine', '.hbs');
    exports.app.set('views', globalopts.projectRoot);
    const addToTemplate = opts.addToTemplate || (() => ({}));
    const oldRender = exports.app.response.render;
    //override so we add the default data
    exports.app.response.render = function (path, data = {}, ...other) {
        data = Object.assign(data, addToTemplate());
        return oldRender.bind(this)(path, data, ...other);
    };
    exports.app.response.renderRouteView = function (path, data = {}) {
        //fixme -- depending on route
        let url = this.req.originalUrl.split('/').slice(1);
        let absolute = Path.resolve(globalopts.routesDir, url[1], 'views', path);
        let relative = Path.relative(globalopts.projectRoot, absolute);
        return this.render(relative, data);
    };
    exports.app.response.renderGlobalView = function (path, data = {}) {
        let absolute = Path.resolve(globalopts.routesDir, '_views', path);
        let relative = Path.relative(globalopts.projectRoot, absolute);
        return this.render(relative, data);
    };
    exports.app.use((error, req, res, next) => {
        if (error.code == 'UNAUTHORIZED')
            return res.redirect('/pages/user/login');
        console.error('Error on route ' + req.url, error);
        res.render('@error', { error });
    });
};
