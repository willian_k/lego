"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer = require("nodemailer");
const marked = require("marked");
const request = require("request");
const promisify_1 = require("promisify");
$usesEnv(__dirname, ['APP_EMAIL_METHOD', 'APP_EMAIL_FROMADDR']);
let METHOD = process.env.APP_EMAIL_METHOD || '';
METHOD = String(METHOD).toUpperCase();
// init
var __choose;
if (METHOD === 'SMTP') {
    $usesEnv(__dirname, ['APP_EMAIL_USER', 'APP_EMAIL_PASS']);
    //use a 3rd-party email service like mailgun in production
    var __transporter = nodemailer.createTransport({
        service: 'Hotmail',
        auth: {
            user: process.env.APP_EMAIL_USER,
            pass: process.env.APP_EMAIL_PASS
        }
    });
    __choose = smtp;
}
else if (METHOD === 'MAILGUN') {
    var domain = process.env.APP_EMAIL_MAILGUN_DOMAIN;
    var from = process.env.APP_EMAIL_FROMADDR;
    var key = process.env.APP_EMAIL_MAILGUN_KEY;
    if (!domain || !from || !key)
        throw Error('Missing mailgun config env vars.');
    __choose = mailgun;
}
else {
    throw Error('Must specify an email method through APP_EMAIL_METHOD.');
}
function smtp({ email, subject, content }) {
    return new Promise((resolve, reject) => {
        __transporter.sendMail({
            from: process.env.APP_EMAIL_USER,
            to: email,
            subject,
            text: content,
            html: marked(content),
        }, (err, info) => {
            if (err)
                return reject(err);
            return resolve(info);
        });
    });
}
function mailgun({ email, subject, content }) {
    return __awaiter(this, void 0, void 0, function* () {
        var domain = process.env.APP_EMAIL_MAILGUN_DOMAIN;
        var from = process.env.APP_EMAIL_FROMADDR;
        var [html] = yield promisify_1.promisify(marked, content);
        var [response, body] = yield promisify_1.promisify(request, {
            method: 'POST',
            auth: {
                user: 'api',
                pass: process.env.APP_EMAIL_MAILGUN_KEY
            },
            url: `https://api.mailgun.net/v3/${domain}/messages`,
            form: {
                from,
                to: email,
                subject,
                text: content,
                html: '<html>' + html + '</html>'
            }
        });
        if (String(response.statusCode).charAt(0) !== '2') {
            throw Error('Falha ao enviar email.');
        }
        return body;
    });
}
function send(params) {
    return __awaiter(this, void 0, void 0, function* () {
        return __choose(params);
    });
}
exports.send = send;
function init() {
    return __awaiter(this, void 0, void 0, function* () { });
}
exports.init = init;
