"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ehbs = require("express-handlebars");
const glob = require("glob");
const Path = require("path");
exports.init = (opts) => {
    let app = opts.app;
    let hbsInstance = ehbs.create({
        extname: '.hbs',
        defaultLayout: opts.defaultLayout || 'main-layout',
        layoutsDir: opts.layoutsDir || opts.routesDir + '/_views/layouts',
        helpers: opts.helpers
    });
    // let moreRoutesDirs = opts.moreRoutesDirs || []
    // let moreDirs = moreRoutesDirs.reduce((out, current) => {
    //     let list = glob.sync(current + '/**/views')
    //     return [...out, ...list]
    // } ,[] as string[])
    hbsInstance.partialsDir = [
        opts.routesDir + './_views/partials',
        ...glob.sync(opts.routesDir + '/**/views'),
    ];
    app.engine('.hbs', hbsInstance.engine);
    app.set('view engine', '.hbs');
    app.set('views', opts.projectRoot);
    const addToTemplate = opts.addToTemplate || (() => ({}));
    const oldRender = app.response.render;
    //override so we add the default data
    app.response.render = function (path, data = {}, ...other) {
        data = Object.assign(data, addToTemplate());
        return oldRender.bind(this)(path, data, ...other);
    };
    app.response.renderRouteView = function (path, data = {}) {
        //fixme -- depending on route
        let url = this.req.originalUrl.split('/').slice(1);
        let absolute = Path.resolve(opts.routesDir, url[1], 'views', path);
        let relative = Path.relative(opts.projectRoot, absolute);
        return this.render(relative, data);
    };
    app.response.renderGlobalView = function (path, data = {}) {
        let absolute = Path.resolve(opts.routesDir, '_views', path);
        let relative = Path.relative(opts.projectRoot, absolute);
        return this.render(relative, data);
    };
};
