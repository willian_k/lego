"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const expressSession = require("express-session");
const _MongoStore = require("connect-mongo");
const MongoStore = _MongoStore(expressSession);
const mongoose = require("mongoose");
exports.initSession = expressSession({
    secret: 'unusualSecret',
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7
    },
    saveUninitialized: false,
    store: new MongoStore({
        mongooseConnection: mongoose.connection,
        ttl: 7 * 24 * 60 * 6
    })
});
exports.authGate = (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (req.session.userId)
        return res.next();
    throw new res.Error(400, { code: 'UNAUTHORIZED' }, 'Não autorizado.');
});
function init() {
    return __awaiter(this, void 0, void 0, function* () { });
}
exports.init = init;
