"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const envVars = {};
function usesEnv(dirname, envVars, required = true) {
    envVars.forEach(envVar => {
        if (envVars[envVar])
            throw Error(`env var ${envVar} is already being used by ${envVars[envVar]}`);
        envVars[envVar] = dirname;
        if (required && !process.env[envVar])
            throw Error(`env var ${envVar} required by ${dirname} is not defined`);
    });
}
exports.usesEnv = usesEnv;
function applyModuleShim(basePath) {
    var Module = require('module');
    var originalRequire = Module.prototype.require;
    Module.prototype.require = function (...args) {
        let path = args[0];
        if (path.charAt(0) === '@')
            path = basePath + '/' + path.substr(1);
        args[0] = path;
        return originalRequire.apply(this, args);
    };
}
exports.applyModuleShim = applyModuleShim;
