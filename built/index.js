"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const modules = require("./components/modules");
exports.modules = modules;
const express = require("express");
exports.express = express;
const database = require("./components/database");
const user = require("./components/user");
const pagesMain = require("./components/router-pages");
const apiMain = require("@components/router-api");
const mail = require("./components/mail");
const wrap_mw_1 = require("./components/wrap-mw");
exports.createMwApp = wrap_mw_1.createMwApp;
exports.createMwRouter = wrap_mw_1.createMwRouter;
modules.applyModuleShim(__dirname);
global['$usesEnv'] = modules.usesEnv;
function init(opts) {
    return __awaiter(this, void 0, void 0, function* () {
        let { app } = opts;
        yield database.init();
        yield mail.init();
        yield user.init();
        yield opts.defineRoutes(pagesMain.app, apiMain.router);
        yield pagesMain.init(opts);
        yield apiMain.init();
        app.use('/pages', pagesMain.app);
        app.use('/api', apiMain.router);
    });
}
exports.init = init;
const mw = require("promise-mw-2");
exports.mw = mw;
const mongoose = require("mongoose");
exports.mongoose = mongoose;
const Router = express.Router;
exports.Router = Router;
