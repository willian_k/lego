import * as express from 'express'
import * as ecore from 'express-serve-static-core'

declare global {

    function $usesEnv(dirname: string, key: string[], required?: boolean)

    namespace Express {

        export interface Response {
            renderRouteView(path: string, opts: any): void
            renderGlobalView(path: string, opts: any): void
            req: express.Request
        }

        export interface Application {
            response : express.Response
        }

    }


    namespace Menolith {

        export type initT = {
            app: express.Application,
            dirname: string,
            projectRoot: string,
            commonFolders: Map<string, string>,
            routesDir: string,
            defineRoutes: (pagesApp: express.Application, apiApp: express.Router) => Promise<any>

            pages?: pagesInitOpts
        }

        export type pagesInitOpts = {
            layoutsDir?: string,
            moreRoutesDirs?: string[],
            helpers?: any
            defaultLayout?: string
            addToTemplate?: () => any
        }

    }

}