import modules = require('./components/modules')
import express = require('express')
import database = require('./components/database')
import user = require('./components/user')
import pagesMain = require('./components/router-pages')
import apiMain = require('@components/router-api')
import mail = require('./components/mail')
import boot = require('./components/app-boot')
import { createMwApp, createMwRouter } from './components/wrap-mw'

modules.applyModuleShim(__dirname)
global['$usesEnv'] = modules.usesEnv

export async function init(opts: Menolith.initT) {
    let { app } = opts
    await database.init()
    await mail.init()
    await user.init()

    await opts.defineRoutes(pagesMain.app, apiMain.router)

    await pagesMain.init( opts )
    await apiMain.init()

    app.use('/pages', pagesMain.app)
    app.use('/api', apiMain.router)
}


import mw = require('promise-mw-2')
import mongoose = require('mongoose')
const Router = express.Router;

export {
    modules,
    express,
    Router,
    mw,
    createMwRouter,
    createMwApp,
    mongoose
}