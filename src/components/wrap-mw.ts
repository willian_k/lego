import mw = require('promise-mw-2')
import express = require('express')

export const createMwRouter
    : () => mw.wrapT & { router: express.Router }
    = () => {
        const router = express.Router()
        let out: any = mw.wrap(router)
        out.router = router
        return out
    }

export const createMwApp
    : () => mw.wrapT & { app: express.Application }
    = () => {
        const app = express()
        const out: any = mw.wrap(app)
        out.app = app
        return out
    }