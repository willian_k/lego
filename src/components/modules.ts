const envVars: { [s: string]: string } = {}

export function usesEnv(dirname: string, envVars: string[], required = true) {
    envVars.forEach(envVar => {
        if (envVars[envVar]) throw Error(`env var ${envVar} is already being used by ${envVars[envVar]}`)
        envVars[envVar] = dirname
        if (required && !process.env[envVar])
            throw Error(`env var ${envVar} required by ${dirname} is not defined`)
    })
}

export function applyModuleShim(basePath) {
    var Module = require('module');
    var originalRequire = Module.prototype.require;

    Module.prototype.require = function(this:any, ...args){
        let path = args[0]
        if (path.charAt(0) === '@') path = basePath + '/' + path.substr(1)
        args[0] = path
        return originalRequire.apply(this, args);
    };
}