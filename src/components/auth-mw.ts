import expressSession = require('express-session')
import _MongoStore = require('connect-mongo')
const MongoStore = _MongoStore(expressSession)
import mongoose = require('mongoose')
import mw = require('promise-mw-2')

export var initSession = expressSession({
    secret: 'unusualSecret',
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7
    },
    saveUninitialized: false,
    store: new MongoStore({
        mongooseConnection: mongoose.connection,
        ttl: 7 * 24 * 60 * 6
    })
})

import express = require('express')


export const authGate : mw.mwGenerateFnT = async (req, res) => {
    if (req.session!.userId) return res.next()
    throw new res.Error(400, {code: 'UNAUTHORIZED'}, 'Não autorizado.')
}

export async function init() { }