import roles = require('./roles')
import express = require('express')
import api = require('./api')
import pages = require('./pages')
import routerPages = require('@components/router-pages')
import routerApi = require('@components/router-api')


export async function init() {
    await roles.init()

    routerPages.app.use('/user', pages._router)
    routerApi.router.use('/user', pages._router)
}