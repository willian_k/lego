import roles = require('./roles')
import express = require('express')
import mw = require('promise-mw-2')
import api = require('./api')
import { authGate } from '@components/auth-mw'
export const _router : express.Router = express.Router()
const user = mw.wrap(_router)


user.get('/login', async (req, res) => {
    if (req.session!.userId) return res.other('redirect', '/pages/user/login')
    return res.render('v-login', {
        title: 'Login',
        //show new account button if theres no admin user or if the option is enabled
        showNewAccount: /*config.new_account_button ||*/ roles.USER_ADMIN_ID === undefined
    })
})


user.get('/password-reset-request', async (req, res) => {
    return res.render('v-password-reset-request')
})


user.get('/password-reset-confirm/:token', async (req, res) => {
    res.ensureHas(req.params, ['token'])
    const token = req.params.token
    let password = await api.passwordResetConfirm(token)
    return res.render('v-password-reset-confirm', { password })
})


user.get('/password-change', authGate, async (req, res) => {
    return res.render('v-password-change', { title: 'Alterar senha' })
})