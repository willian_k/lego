import mongoose = require('mongoose')

export var ROLE_ADMIN_ID
export var ROLE_NEW_ID
export var USER_ADMIN_ID

export interface MongoLean {
    _id?: string
}

//role -----

const roleSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    isOwner: Boolean,
    isNew: Boolean
})

export interface RoleType extends MongoLean {
    name: string
    isOwner?: boolean
    isNew?: boolean
}

export const Role = mongoose.model('Role', roleSchema)


//user -----

export interface UserType extends MongoLean {
    role: string
    hash: string
    name: string
    email: string
}

export const User = mongoose.model<UserType & mongoose.Document>(
    'User',
    new mongoose.Schema({
        role: {
            type: mongoose.Types.ObjectId,
            required: true
        },
        hash: String,
        name: String,
        email: String
    }
    )
)


//event token ----


export interface EventTokenType extends MongoLean {
    user: string
    event: 'password-reset-confirm'
    token: string
}


export const EventToken = mongoose.model<EventTokenType & mongoose.Document>(
    'EventToken',
    new mongoose.Schema({
        user: { type: String, required: true },
        event: { type: String, required: true },
        token: { type: String, required: true }
    })
)



//init -----


export async function init() {
    //create admin role
    let rows = await Role.find({}).count().exec()
    if (!rows) {
        console.log('Initializing roles (first run)...')
        Role.create({
            name: 'admin',
            isOwner: true,
            isNew: true
        })
    }

    //define who has the key roles
    let rows2 = await Role.find({}).lean().exec() as RoleType[]
    rows2.forEach(row => {
        if (row.isOwner) ROLE_ADMIN_ID = row._id
        else if (row.isNew) ROLE_NEW_ID = row._id
    })
    let rows3 = await User.find({ role: ROLE_ADMIN_ID }).lean().exec() as UserType[]
    if (rows3.length) USER_ADMIN_ID = rows3[0]._id
}