import express = require('express')
import mw = require('promise-mw-2')
import roles = require('./roles')
import bcrypt = require('bcrypt')
import { authGate } from '@components/auth-mw'
import { promisify } from 'promisify'
import passwordGen = require('password-generator')
import uuid = require('uuid/v1')
import mailer = require('@components/mail')

export const _router : express.Router = express.Router()
const api = mw.wrap(_router)
const HTTP = mw.STATUS_CODES

async function userLogin({ username, password, session }) {
    if (!username || !password) throw Error('Falta o nome de usuário ou senha.')
    if (session.userId) throw Error('Primeiro faça o logout da sessão atual.')

    let [found] = await roles.User.find({ name : username}).select({ hash : 1 }).limit(1).exec()
    if (!found) throw new mw.Error(HTTP.UNAUTHORIZED, 'Credenciais inválidas.')

    let result = await bcrypt.compare(password, found.hash)
    if (!result) throw new mw.Error(HTTP.UNAUTHORIZED, 'Credenciais inválidas.')
    session.userId = found._id
    session.userName = found.name
    return { status: 'OK' }
}


api.post('/login', async (req, res) => {
    res.ensureHas(req.body, ['username', 'password'])
    let resp = await userLogin({
        username: req.body.username,
        password: req.body.password,
        session: req.session
    })
    return res.send(resp)
})

api.get('/logout', authGate, async (req, res) => {
    await promisify(req.session!.destroy)
    return res.send({ status: 'OK' })
})


export async function passwordResetConfirm(_token) {
    let [event] = await roles.EventToken.find({ event: 'pasword-reset-confirm', token: _token }).exec()
    if (!event) throw Error('Falha ao encontrar token para esse pedido.')
    let newpass = passwordGen(12)
    let newHash = await bcrypt.hash(newpass, 10)
    await roles.User.update({ _id: event._id }, { $set: { hash: newHash } }).exec()
    await event.remove()
    return newpass
}


api.post('/password-reset-confirm', async (req, res) => {
    res.ensureHas(req.body, ['token'])
    let newpass = await passwordResetConfirm(req.body.token)
    return res.send({ password: newpass })
})



api.post('/password-reset-request', async (req, res) => {
    res.ensureHas(req.body, ['email'])

    type joinedT = roles.UserType & { EventToken : roles.EventTokenType[] }
    const [user] : joinedT = await roles.User.find({ email: req.body.email }).aggregate([
        {
            $lookup: {
                from: 'User',
                localField: "_id",
                foreignField: 'user',
                as : 'EventToken'
            }
        }
    ]).exec()

    if (!user) return res.send({})
    if (!user.token) throw Error('Já existe um pedido pendente para este usuário.')
    const tokenToSave = uuid()
    await new roles.EventToken(<roles.EventTokenType>{
        user: user._id,
        event: 'password-reset-confirm',
        token: tokenToSave
    }).save()

    await mailer.send({
        email: req.body.email,
        subject: 'Redefinição de senha',
        content: PWD_RESET_CONTENT(user.name, tokenToSave)
    })

    return res.send({ token: tokenToSave })
})

const PWD_RESET_CONTENT = (username, bytes) =>
`**Olá ${username}**,
Acesse o link ${process.env.APP_BASE_URL}/pages/user/password-reset-confirm/${bytes}
para redefinir a sua senha.`



api.post('/create', async (req, res) => {
    if (roles.USER_ADMIN_ID !== undefined) throw Error('No momento, não é possível adicionar um novo editor.')
    res.ensureHas(req.body, ['username', 'email'])
    let { username, email } = req.body
    if (!username || !email) throw Error('Falta o nome de usuário ou o email.')
    let _password = passwordGen(12)
    let hash = await bcrypt.hash(_password, 10)
        //TODO: multiple editors
    let setRole = roles.USER_ADMIN_ID === undefined ? roles.ROLE_ADMIN_ID : roles.ROLE_NEW_ID
    await new roles.User(<roles.UserType>{
        name: username, email: email, hash: hash, role: setRole
    }).save()
    await mailer.send({
        email,
        subject: 'Nova conta',
        content: NEW_USER_CONTENT(username, _password)
    })
    return res.send({ status: 'ok' })
})


const NEW_USER_CONTENT = (username, _password) =>
`Olá! Essas são suas credenciais:
**Nome de usuário**: ${username}
**Senha:** ${_password}`


api.post('/password-change', authGate, async (req, res) => {
    var b = req.body
    res.ensureHas(b, ['currentPassword', 'newPassword', 'newPasswordConfirm'])
    if (b.newPassword !== b.newPasswordConfirm) throw Error('As novas senhas devem bater.')
    let [user] = await roles.User.find({ _id: req.session!.userId }).select({ hash: 1 }).limit(1).exec()
    if (!user) throw Error('Erro Inesperado!')
    let compareresult = bcrypt.compare(b.currentPassword, user.hash)
    if (!compareresult) throw Error('Senha incorreta.')
    let hashed = bcrypt.hash(b.newPassword, 10)
    await user.update({ $set: { hash :hashed }})
    return res.send({ status : 'ok' })
})
