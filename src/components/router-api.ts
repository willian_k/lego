import bodyParser = require('body-parser')
import auth = require('@components/auth-mw')
import express = require('express')

export const router : express.Router = express.Router()
router.use(bodyParser.json())
router.use('/user', require('./user/api'))


export async function init() {

    router.use((error, req, res, next) => {
        console.error(error)
        res.status(error.statusCode || 500).send({
            error: {
                message: error.message
            }
        })
    })

}