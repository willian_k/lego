import mongoose = require('mongoose')
$usesEnv(__dirname, ['DB_USER', 'DB_PASS', 'DB_NAME'])

export async function init() {
    let opts = {
        user: process.env.DB_USER,
        pass: process.env.DB_PASS
    }
    await <any>mongoose.connect(`mongodb://127.0.0.1/${process.env.DB_NAME}`, opts)
}