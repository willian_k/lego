import express = require('express')
import ehbs = require('express-handlebars')
import glob = require('glob')
import Path = require('path')


export const app : express.Application = express()

export const init
    : (globalopts: Menolith.initT) => void
    = (globalopts) => {

        const opts = globalopts.pages || {}

        let hbsInstance: any = ehbs.create({
            extname: '.hbs',
            defaultLayout: opts.defaultLayout || 'main-layout',
            layoutsDir: opts.layoutsDir || globalopts.routesDir + '/_views/layouts',
            helpers: opts.helpers
        })


        // let moreRoutesDirs = opts.moreRoutesDirs || []
        // let moreDirs = moreRoutesDirs.reduce((out, current) => {
        //     let list = glob.sync(current + '/**/views')
        //     return [...out, ...list]
        // } ,[] as string[])


        hbsInstance.partialsDir = [
            globalopts.routesDir + './_views/partials', //global partials,
            ...glob.sync(globalopts.routesDir + '/**/views'),
            //...moreDirs
        ]

        app.engine('.hbs', hbsInstance.engine)
        app.set('view engine', '.hbs')
        app.set('views', globalopts.projectRoot)

        const addToTemplate = opts.addToTemplate || (() => ({}))
        const oldRender = app.response.render

        //override so we add the default data
        app.response.render = function (this: express.Response, path: string, data: any = {}, ...other) {
            data = Object.assign(data, addToTemplate())
            return oldRender.bind(this)(path, data, ...other)
        }


        app.response.renderRouteView = function (this: express.Response, path: string, data:any = {}) {
            //fixme -- depending on route
            let url = this.req.originalUrl.split('/').slice(1)

            let absolute = Path.resolve(globalopts.routesDir, url[1], 'views', path)
            let relative = Path.relative(globalopts.projectRoot, absolute)
            return this.render( relative, data )
        }


        app.response.renderGlobalView = function (this: express.Response, path: string, data: any = {}) {
            let absolute = Path.resolve(globalopts.routesDir, '_views', path)
            let relative = Path.relative(globalopts.projectRoot, absolute)
            return this.render(relative, data)
        }


        app.use((error, req, res, next) => {
            if (error.code == 'UNAUTHORIZED')
                return res.redirect('/pages/user/login')

            console.error('Error on route ' + req.url, error)
            res.render('@error', { error })
        })

    }