export declare type MailParams = {
    email;
    subject;
    content;
};
export declare function send(params: MailParams): Promise<any>;
export declare function init(): Promise<void>;
