/// <reference types="express" />
import mw = require('promise-mw-2');
import express = require('express');
export declare const createMwRouter: () => mw.wrapT & {
    router: express.Router;
};
export declare const createMwApp: () => mw.wrapT & {
    app: express.Application;
};
