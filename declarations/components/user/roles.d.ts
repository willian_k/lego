/// <reference types="mongoose" />
import mongoose = require('mongoose');
export declare var ROLE_ADMIN_ID: any;
export declare var ROLE_NEW_ID: any;
export declare var USER_ADMIN_ID: any;
export interface MongoLean {
    _id?: string;
}
export interface RoleType extends MongoLean {
    name: string;
    isOwner?: boolean;
    isNew?: boolean;
}
export declare const Role: mongoose.Model<mongoose.Document>;
export interface UserType extends MongoLean {
    role: string;
    hash: string;
    name: string;
    email: string;
}
export declare const User: mongoose.Model<UserType & mongoose.Document>;
export interface EventTokenType extends MongoLean {
    user: string;
    event: 'password-reset-confirm';
    token: string;
}
export declare const EventToken: mongoose.Model<EventTokenType & mongoose.Document>;
export declare function init(): Promise<void>;
