/// <reference types="express" />
import express = require('express');
export declare const _router: express.Router;
export declare function passwordResetConfirm(_token: any): Promise<string>;
