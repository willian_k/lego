/// <reference types="express" />
import modules = require('./components/modules');
import express = require('express');
import { createMwApp, createMwRouter } from './components/wrap-mw';
export declare function init(opts: Menolith.initT): Promise<void>;
import mw = require('promise-mw-2');
import mongoose = require('mongoose');
declare const Router: typeof express.Router;
export { modules, express, Router, mw, createMwRouter, createMwApp, mongoose };
